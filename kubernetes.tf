resource "google_container_cluster" "devops" {
  name     = "gke"
  location = "asia-southeast1"
  min_master_version = "1.15.12-gke.2"
  network  = "devops"
  subnetwork = "kubernetes"
  remove_default_node_pool = true
  initial_node_count = 1
  #master_authorized_networks_config {
  #  }
 # private_cluster_config {
 #       enable_private_nodes = true
 #       enable_private_endpoint = true
 #       master_ipv4_cidr_block = "172.168.1.0/28"
 #   }
  ip_allocation_policy {
      cluster_ipv4_cidr_block = "10.0.0.0/21"
      #create_subnetwork = true
 }
}

resource "google_container_node_pool" "devops_node" {
  name       = "gke-node"
  location   = "asia-southeast1"
  cluster    = google_container_cluster.devops.name
  node_count = 1
  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"

    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}