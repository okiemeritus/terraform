resource "google_compute_instance" "jenkins" {
  name       = "jenkins-server"
  zone       = "asia-southeast1-a"
  tags       = ["devops"]
  machine_type = "n1-standard-1"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }
  network_interface {
    network = "devops"

    access_config {
      // Ephemeral IP
    }
  }
}
resource "google_compute_disk" "disk01" {
  name  = "disk01"
  type  = "pd-standard"
  zone  = "asia-southeast1-a"
  size  = "1000"
}