provider "google" {
  project = "noeng-project"
  zone =  "asia-southeast1-a"
  credentials = "${file("noeng-project-c7a902ba3582.json")}"
}
