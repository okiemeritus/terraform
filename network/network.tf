resource "google_compute_network" "devops" {
  name = "devops"
}
resource "google_compute_subnetwork" "kubernetes" {
  name          = "kubernetes"
  ip_cidr_range = "192.168.0.0/16"
  region        = "asia-southeast1"
  network       = "${google_compute_network.devops.self_link}"
}
resource "google_compute_firewall" "devops" {
  name = "devops"
  network = "${google_compute_network.devops.self_link}"
  allow {
    protocol = "icmp"
  }
  allow{
    protocol = "tcp"
    ports = ["22", "80", "443", "31000"]
  }
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_router" "router" {
  name    = "router"
  region  = "asia-southeast1"
  network = "devops"
}

resource "google_compute_router_nat" "nat" {
  name                               = "router-nat"
  router                             = google_compute_router.router.name
  region                             = "asia-southeast1"
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}